# Overview

A technical interview project for data engineers.

The objective is to write a python program that will collect as many logos as you can across across a sample of websites.


# Objectives

* Write a program that will crawl a list of website and output their logo URLs.
* The program should read domain names on `STDIN` and write a CSV of domain and logo URL to `STDOUT`.
* A `websites.csv` list is included as a sample to crawl.
* You can't always get it right, but try to keep precision and recall as high as you can. Be prepared to explain ways you can improve. Bonus points if you can measure.
* Be prepared to discuss the bottlenecks as you scale up to millions of websites. You don't need to implement all the optimizations, but be able to talk about the next steps to scale it for production.
* Favicons aren't an adequate substitute for a logo, but if you choose, it's also valuable to extract as an additional field.
* Spare your time on implementing features that would be time consuming, but make a note of them so we can discuss the ideas.
* Please implement using python.
* Please keep 3rd party dependencies to a minimum, unless you feel there's an essential reason to add a dependency.
* We use [Nix](https://nixos.org/nix/) for package management. If you add your dependencies to `default.nix`, then it's easy for us to run your code. Install nix and launch the environment with `nix-shell` (works on Linux, macOS, and most unixes).

There's no time limit. Spend as much or as little time on it as you'd like. Clone this git repository (don't fork), and push to a new repository when you're ready to share. We'll schedule a follow-up call to review.

# Usage
To run the program that will crawl a list of URLs just lauch the nix environment and then run:

```bash
python main.py [-h] --urls [URLS ...] [--timeout TIMEOUT] [--n-procs N_PROCS] [--reports-file REPORTS_FILE]
```
Where,

| Argument     | Input/Format  | Default value | Description |  
| -            | -             | -             | - |                                   
| urls         | `list`        | -             | List of urls to crawl. |  
| n-procs      | `int`         | `20`          | How many parallel processes to use. |  
| reports-file | `str`         | `reports.txt` | File to write the logo/favicon estimate recalls (**If file already exists it will be overwritten**). |  
| timeout      | `int`         | `1`           | Max time (in s) to wait for a website request. |  

## Run with the given CSV as input
To pass all websites from the given CSV file (`websites.csv`) as input, just use the program with:
```bash
python main.py [-h] --urls $(cat websites.csv) [--timeout TIMEOUT] [--n-procs N_PROCS] [--reports-file REPORTS_FILE]
```

## Save output to a CSV file
To save the output in a CSV file (e.g. `logos.csv`), just use the program with:
```bash
python main.py [-h] --urls $(cat websites.csv) [--timeout TIMEOUT] [--n-procs N_PROCS] [--reports-file REPORTS_FILE] > logos.csv
```

# Next Steps
There are some enhancements that could be done to this code, we list some of them bellow:
* [ ] Be able to decode svg images in `<svg>` tags
* [ ] Also crawl the images that are called in CSS and JS files 
* [ ] Deal with relative paths related to images in CSS and JS files
* [ ] Be able to get images in JSON-LD
* [ ] Use the `isValidImage` method in the best image/favicon choosing process