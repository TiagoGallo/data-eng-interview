import argparse
import requests
import re
import os 
from html.parser import HTMLParser
from multiprocessing import Process, Queue

#HTML request headers
headers = {
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
    'accept-language': 'en-US,en;q=0.9,fr;q=0.8,ro;q=0.7,ru;q=0.6,la;q=0.5,pt;q=0.4,de;q=0.3',
    'cache-control': 'max-age=0',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36' + '(KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36'
}

def isValidUrl(url): 
    """Check if an image URL is complete or if it need to be joined with the base website URL"""
    if 'http' in url:
        return True
    return False

def join_urls(base_url, image_url):
    """Join the base website URL with the image relative path"""
    if 'cdn' in image_url:
        return image_url[image_url.find('cdn'):]
    elif base_url[-1] == '/' and image_url [0] == '/':
        return base_url + image_url[1:]
    return base_url + image_url        

class MyHTMLParser(HTMLParser):
    """Class to parse a HTML file"""
    def __init__(self):
        super().__init__()
        self.base_url = None
        self.images = []
        self.icons = []
        self.brand = None

    def reset_parser(self, base_url):
        self.base_url = base_url
        self.images = []
        self.icons = []
        self.brand = base_url.split('.')[-2].lower()

    def add_image(self, imageName):
        match = re.search(r'/([\w_.+-]+[.](jpe?g|png|svg|ico))', imageName)
        if match is None:
            return
        self.images.append(imageName if isValidUrl(imageName) else join_urls(self.base_url, imageName))
        
    def add_icon(self, icon):
        match = re.search(r'/([\w_-]+[.](jpe?g|png|svg|ico))', icon)
        if match is None:
            return
        self.icons.append(icon if isValidUrl(icon) else join_urls(self.base_url, icon))

    def handle_starttag(self, tag, attrs):
        attributes = {}
        for attr in attrs:
            attributes[attr[0]] = attr[1]
        
        try:
            #Search for logo images in a <img> tag
            if tag == 'img':
                for key, data in attributes.items():
                    if (key == 'itemprop' and data == 'logo') or \
                    (key.startswith('alt') and attributes[key] == 'logo')   or \
                    (key.startswith('class') and attributes[key] == 'logo') or \
                    (key.startswith('src') and attributes[key] == 'logo')   or \
                    (key == 'src' and 'logo' in data) or \
                    (key == 'src' and 'icon' in data) or \
                    (key == 'src' and self.brand in data):

                       self.add_image(attributes['src'])
            
            elif tag == 'meta':
                #Search for logo images in a <meta> tag
                for key, data in attributes.items():
                    if (key == 'property' and data == 'og:logo') or \
                    (key == 'property' and data == 'og:image') or \
                    (key == 'itemprop' and data == 'logo') or \
                    (key.startswith('content') and attributes[key] == 'logo') or \
                    (key.startswith('itemprop') and attributes[key] == 'image')   or \
                    (key.startswith('name') and attributes[key] == 'msapplication-TileImage')   or \
                    (key == 'content' and 'logo' in data) or \
                    (key == 'content' and 'icon' in data) or \
                    (key == 'content' and self.brand in data):

                       self.add_image(attributes['content'])
            
            elif tag == 'link':
                #Search for favicon images in a <link> tag
                for key in list(attributes.keys()):
                    if key.startswith('rel') and attributes[key] == 'icon':
                        self.add_icon(attributes['href'])
                        return

        except Exception:
            pass

def get_html(base_url, max_timeout):
    """
        Get the website HTML

    Args:
        base_url (str): The base website HTML provided as input
        max_timeout (int): Max time (in s) to wait for a website request

    Raises:
        Exception: Could not get the website 

    Returns:
        str, str: The website HTML code and the correct website url
    """
    try:
        r = requests.get(f'http://{base_url}', headers=headers, timeout=max_timeout)
    except Exception:
        try:
            r = requests.get(f'https://{base_url}', headers=headers, timeout=max_timeout)
        except Exception:
            raise Exception
    
    return r.text, r.url

def isValidImage(best_image):
    """Check if an image URL is reachable"""
    r_image = requests.get(best_image, timeout=1)

    if r_image.status_code == 200:
        return True
    else:
        return False

def choose_better_image_and_icon(parser):
    """Choose the image and the favicon that are most probable to be correct"""
    best_image, best_icon = '', ''

    # Use the first image as a placeholder
    if len(parser.images) > 0:
        best_image = parser.images[0]
    
    # If there are favicons available, we always choose the first one
    if len(parser.icons) > 0:
        best_icon = parser.icons[0]

    # Try to find images where the 'logo' or the website brand is in the image name
    for img in parser.images:
        if ('logo' in img.split('/')[-1].lower()) or (parser.brand in img.split('/')[-1].lower()):
            best_image = img
            break

    return best_image, best_icon

class Logo_Crawler:
    """Class to crawl the logo from a urls list in a separete process"""
    def __init__(self, urls_list, index):
        self.urls_list = urls_list
        
        self.Q = Queue(maxsize=1)
        self.name = index

        self.parser = MyHTMLParser()

        self.report = {
            'Urls_crawled': 0,
            'Logos_found': 0,
            'Favicons_found': 0,
            'Conn_errors': 0,
            'Logos_false_positives': 0,
            'Favicons_false_positives': 0,
        }

    def start(self):
        # start the process
        self.P = Process(target=self.loop, name=self.name)
        self.P.daemon = True
        self.P.start()
        
        return self

    def update_report(self, best_image, best_icon):
        """
            Update the results report

        Args:
            best_image (str): URL to the best_image
            best_icon (str): URL to the best_icon
        """
        self.report['Urls_crawled'] += 1
        if best_image != '':
            self.report['Logos_found'] += 1
            if not isValidImage(best_image):
                self.report['Logos_false_positives'] += 1
        if best_icon != '':
            self.report['Favicons_found'] += 1
            if not isValidImage(best_icon):
                self.report['Favicons_false_positives'] += 1

    def loop(self):
        """Loop over the URLs and search for a logo and a favicon in them"""
        for url in self.urls_list:
            try:
                html, correct_url = get_html(url, args['timeout'])
                self.parser.reset_parser(correct_url)
                self.parser.feed(html)
                best_image, best_icon = choose_better_image_and_icon(self.parser)
                print(f'{correct_url},{best_image},{best_icon}')
                self.update_report(best_image, best_icon)
            
            except Exception as e:
                print(f'{url},,')
                self.report['Conn_errors'] += 1
                self.report['Urls_crawled'] += 1
                continue

        self.Q.put(self.report)
        
        #Waiting for the main process to get the queue info
        while not self.Q.empty():
            pass

def print_report(all_reports, reports_file):
    """
        Print to reports_file the recall and precision informations

    Args:
        all_reports (list): List of report dicts from every processes
        reports_file (str): Path to the output reports file
    """
    #Merge all reports dictionary into one dict
    report = {
            'Urls_crawled': 0,
            'Logos_found': 0,
            'Favicons_found': 0,
            'Conn_errors': 0,
            'Logos_false_positives': 0,
            'Favicons_false_positives': 0,
        }

    for rep in all_reports:
        report['Urls_crawled'] += rep['Urls_crawled']
        report['Logos_found'] += rep['Logos_found']
        report['Favicons_found'] += rep['Favicons_found']
        report['Conn_errors'] += rep['Conn_errors']
        report['Logos_false_positives'] += rep['Logos_false_positives']
        report['Favicons_false_positives'] += rep['Favicons_false_positives']

    #Always write to a new file, delete if already exists
    if os.path.isfile(reports_file):
        os.remove(reports_file)

    # Calc the metrics and write them to the reports file
    with open(reports_file, 'w') as rep_file:
        rep_file.write(f'Considering that:\n\t1) All logos and favicons found (and reachable) were correct\n\t2) All websites have a logo and a favicon\n')
        logo_recall = (report['Logos_found'] - report['Logos_false_positives']) / report['Urls_crawled']
        favicon_recall = (report['Favicons_found'] - report['Favicons_false_positives']) / report['Urls_crawled']

        logo_precision = (report['Logos_found'] - report['Logos_false_positives']) / report['Logos_found']
        favicon_precision = (report['Favicons_found'] - report['Favicons_false_positives']) / report['Favicons_found']
        rep_file.write(f'\t\tLogos Recall: {logo_recall*100:.2f}%\n\t\tFavicon Recall: {favicon_recall*100:.2f}%\n')
        rep_file.write(f'\t\tLogos Precision: {logo_precision*100:.2f}%\n\t\tFavicon Precision: {favicon_precision*100:.2f}%\n\n')

        rep_file.write(f'Considering that:\n\t1) All logos and favicons found (and reachable) were correct\n\t'
                        '2) All websites have a logo and a favicon\n\t'
                        f'3) Disconsidering the websites where we had connection errors ({report["Conn_errors"]} websites)\n')
        logo_recall = (report['Logos_found'] - report['Logos_false_positives']) / (report['Urls_crawled'] - report['Conn_errors'])
        favicon_recall = (report['Favicons_found'] - report['Favicons_false_positives']) / (report['Urls_crawled'] - report['Conn_errors'])
        rep_file.write(f'\t\tLogos Recall: {logo_recall*100:.2f}%\n\t\tFavicon Recall: {favicon_recall*100:.2f}%\n')
        rep_file.write(f'\t\tLogos Precision: {logo_precision*100:.2f}%\n\t\tFavicon Precision: {favicon_precision*100:.2f}%')

if __name__ == '__main__':
    ap = argparse.ArgumentParser()

    ap.add_argument('--urls', nargs='*', required=True,
                    help='List of urls to crawl')
    ap.add_argument('--timeout', type=int, default=1,
                    help='Max time (in s) to wait for a website request')
    ap.add_argument('--n-procs', default=20, type=int,
                    help='How many parallel processes to use')
    ap.add_argument('--reports-file', default='reports.txt',
                    help='File to write the logo/favicon estimate recalls (If file already exists it will be overwritten)')

    args = vars(ap.parse_args())

    urls = args['urls']
    print('Website,logo_URL,Favicon_URL')

    # Separate the urls list into n_procs almost equal parts and feed them to the processes
    processes = []

    #Start the processes
    avg_list_len = int(len(urls) / args['n_procs'])
    for i in range(args['n_procs']-1):
        proc = Logo_Crawler(urls[i* avg_list_len:(i+1) * avg_list_len], i)
        proc.start()
        processes.append(proc)
    
    proc = Logo_Crawler(urls[(args['n_procs'] - 1)* avg_list_len:], (args['n_procs'] - 1))
    proc.start()
    processes.append(proc)

    # Wait fot the processes process all the data
    processes_finished = [False] * len(processes)
    all_reports = []
    while True:
        for i, thread in enumerate(processes):
            if not thread.Q.empty():
                report = thread.Q.get()
                processes_finished[i] = True
                all_reports.append(report)

        if sum(processes_finished) == len(processes_finished):
            break

    # Print the final results
    print_report(all_reports, args['reports_file'])